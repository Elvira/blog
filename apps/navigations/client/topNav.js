Template.topNav.helpers({
	isAdmin: function() {
		return (Meteor.user() && Meteor.user().profile && (Meteor.user().profile.role === "admin"));
	}
});

Template.topNav.events({
	'touchstart .search': function(e, t) {
		var search = $("#search").val();
		Meteor.call()
	},
	'touchstart .login': function(e, t) {
		Session.set("login", true);
		Router.go("start");
	},
	'touchstart .logout': function(e, t) {
		Meteor.logout();
	},
	'click .search': function(e, t) {
		var search = $("#search").val();
		Meteor.call()
	},
	'click .login': function(e, t) {
		Session.set("login", true);
		Router.go("start");
	},
	'click .logout': function(e, t) {
		Meteor.logout();
	}
});