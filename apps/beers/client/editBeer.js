Template.editBeer.helpers({
	type: function() {
		if (Router.current().params && Router.current().params._id) {
			return "update";
		} else {
			return "insert";
		}
	},
	beer: function() {
		if (Router.current().params && Router.current().params._id) {
			return Beers.findOne({_id: Router.current().params._id});
		}
	}
});