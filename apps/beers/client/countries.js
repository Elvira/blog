Template.countriesList.helpers({
	headEdit: function() {
		if (Session.get("selectedCountryId")) {
			return "Редактировать страну";
		} else {
			return "Добавить новую страну";
		}
	},
	type: function() {
		if (Session.get("selectedCountryId")) {
			return "update";
		} else {
			return "insert";
		}
	},
	country: function() {
		if (Session.get("selectedCountryId")) {
			return Countries.findOne({_id: Session.get("selectedCountryId")});
		}
	},
	countries: function() {
		return Countries.find({}, {sort: {title: 1}});
	}
});

Template.countriesList.events({
	'touchstart .addCountry': function(e, t) {
		Session.set("selectedCountryId", null);
		$(".myModal").modal("show");
	},
	'touchstart .editCountry': function(e, t) {
		Session.set("selectedCountryId", this._id);
		$(".myModal").modal("show");
	},
	'click .addCountry': function(e, t) {
		Session.set("selectedCountryId", null);
		$(".myModal").modal("show");
	},
	'click .editCountry': function(e, t) {
		Session.set("selectedCountryId", this._id);
		$(".myModal").modal("show");
	}
});

AutoForm.hooks({
	country: {
		onSuccess: function(formType, result) {
			$(".myModal").modal("hide");
			Session.set("selectedCountryId", null);
		}
	}
});