Template.beerTypesList.helpers({
	headEdit: function() {
		if (Session.get("selectedBeerTypeId")) {
			return "Редактировать тип";
		} else {
			return "Добавить новый тип";
		}
	},
	type: function() {
		if (Session.get("selectedBeerTypeId")) {
			return "update";
		} else {
			return "insert";
		}
	},
	beerType: function() {
		if (Session.get("selectedBeerTypeId")) {
			return BeerTypes.findOne({_id: Session.get("selectedBeerTypeId")});
		}
	},
	beerTypes: function() {
		return BeerTypes.find({}, {sort: {title: 1}});
	}
});

Template.beerTypesList.events({
	'touchstart .addType': function(e, t) {
		Session.set("selectedBeerTypeId", null);
		$(".myModal").modal("show");
	},
	'touchstart .editType': function(e, t) {
		Session.set("selectedBeerTypeId", this._id);
		$(".myModal").modal("show");
	},
	'click .addType': function(e, t) {
		Session.set("selectedBeerTypeId", null);
		$(".myModal").modal("show");
	},
	'click .editType': function(e, t) {
		Session.set("selectedBeerTypeId", this._id);
		$(".myModal").modal("show");
	}
});

AutoForm.hooks({
	beerType: {
		onSuccess: function(formType, result) {
			$(".myModal").modal("hide");
			Session.set("selectedBeerTypeId", null);
		}
	}
});