Template.beerPackingsList.helpers({
	headEdit: function() {
		if (Session.get("selectedBeerPackingId")) {
			return "Редактировать тип расфасовки";
		} else {
			return "Добавить новый тип расфасовки";
		}
	},
	type: function() {
		if (Session.get("selectedBeerPackingId")) {
			return "update";
		} else {
			return "insert";
		}
	},
	beerPacking: function() {
		if (Session.get("selectedBeerPackingId")) {
			return BeerPackings.findOne({_id: Session.get("selectedBeerPackingId")});
		}
	},
	beerPackings: function() {
		return BeerPackings.find({}, {sort: {title: 1}});
	}
});

Template.beerPackingsList.events({
	'touchstart .addPacking': function(e, t) {
		Session.set("selectedBeerPackingId", null);
		$(".myModal").modal("show");
	},
	'touchstart .editPacking': function(e, t) {
		Session.set("selectedBeerPackingId", this._id);
		$(".myModal").modal("show");
	},
	'click .addPacking': function(e, t) {
		Session.set("selectedBeerPackingId", null);
		$(".myModal").modal("show");
	},
	'click .editPacking': function(e, t) {
		Session.set("selectedBeerPackingId", this._id);
		$(".myModal").modal("show");
	}
});

AutoForm.hooks({
	beerPacking: {
		onSuccess: function(formType, result) {
			$(".myModal").modal("hide");
			Session.set("selectedBeerPackingId", null);
		}
	}
});