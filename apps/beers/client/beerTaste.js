Template.beerTaste.helpers({
	type: function() {
		if (Router.current().params && Router.current().params._id) {
			return "update";
		} else {
			return "insert";
		}
	},
	beerTaste: function() {
		if (Router.current().params && Router.current().params._id) {
			return BeerTastes.findOne({_id: Router.current().params._id});
		} else {
			return {author: Meteor.userId(), beer: Router.current().params.beerId};
		}
	},
	beer: function() {
		return Beers.findOne({_id: Router.current().params.beerId});
	}
});

AutoForm.hooks({
	beerTaste: {
		formToDoc: function(doc) {
			doc.author = Meteor.userId();
			doc.beer = Router.current().params.beerId;
			return doc;
		},
		onSubmit: function (insertDoc, updateDoc, currentDoc) {
			console.log(insertDoc, updateDoc, currentDoc);
			this.done();
			return false;
		}
	}
})