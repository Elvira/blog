Template.registerHelper("countryName", function(countryId) {
	var country = Countries.findOne({_id: countryId});
	if (country) return country.title;
	return "";
});

Template.registerHelper("typeName", function(typeId) {
	var beerType = BeerTypes.findOne({_id: typeId});
	if (beerType) return beerType.title;
	return "";
});

Template.registerHelper("packingName", function(packingId) {
	var beerPacking = BeerPackings.findOne({_id: packingId});
	if (beerPacking) return beerPacking.title;
	return "";
});

Template.registerHelper("photoUrl", function(store, photoId) {
	var photo = Photos.findOne({_id: photoId});
	if (photo) {
		return photo.url({store: store});
	}
	return "";
});