Countries = new Mongo.Collection("countries");

CountrySchema = new SimpleSchema({
	title: {
		type: String,
		label: "Название"
	},
	codeNumberStart: {
		type: String,
		label: "Штрих-код с"
	},
	codeNumberEnd: {
		type: String,
		label: "Штрих-код по"
	}
});

Countries.attachSchema(CountrySchema);

Countries.allow({
	'insert': function(userId, doc) {
		var user = Meteor.users.findOne({_id: userId});
		if (user && user.profile && (user.profile.role == "admin")) return true;
		return false;
	},
	'update': function(userId, doc, fields, modifier) {
		var user = Meteor.users.findOne({_id: userId});
		if (user && user.profile && (user.profile.role == "admin")) return true;
		return false;
	},
	'remove': function(userId, doc) {
		return false;
	}
});

BeerTypes = new Mongo.Collection("beertypes");

BeerTypeSchema = new SimpleSchema({
	title: {
		type: String,
		label: "Название"
	}
});

BeerTypes.attachSchema(BeerTypeSchema);

BeerTypes.allow({
	'insert': function(userId, doc) {
		var user = Meteor.users.findOne({_id: userId});
		if (user && user.profile && (user.profile.role == "admin")) return true;
		return false;
	},
	'update': function(userId, doc, fields, modifier) {
		var user = Meteor.users.findOne({_id: userId});
		if (user && user.profile && (user.profile.role == "admin")) return true;
		return false;
	},
	'remove': function(userId, doc) {
		return false;
	}
});

BeerPackings = new Mongo.Collection("beerpackings");

BeerPackingSchema = new SimpleSchema({
	title: {
		type: String,
		label: "Название"
	}
});

BeerPackings.attachSchema(BeerPackingSchema);

BeerPackings.allow({
	'insert': function(userId, doc) {
		var user = Meteor.users.findOne({_id: userId});
		if (user && user.profile && (user.profile.role == "admin")) return true;
		return false;
	},
	'update': function(userId, doc, fields, modifier) {
		var user = Meteor.users.findOne({_id: userId});
		if (user && user.profile && (user.profile.role == "admin")) return true;
		return false;
	},
	'remove': function(userId, doc) {
		return false;
	}
});

Beers = new Mongo.Collection("beers");

BeerSchema = new SimpleSchema({
	code: {
		type: String,
		label: "Штрих-код",
		unique: true,
		optional: true
	},
	title: {
		type: String,
		label: "Название"
	},
	country: {
		type: String,
		label: "Страна",
		autoform: {
			options: function() {
				return _.map(Countries.find().fetch(), function(c, i) {
					return {label: c.title, value: c._id}
				})
			}
		}
	},
	type: {
		type: [String],
		label: "Тип",
		autoform: {
			options: function() {
				return _.map(BeerTypes.find().fetch(), function(c, i) {
					return {label: c.title, value: c._id}
				})
			}
		}
	},
	packing: {
		type: String,
		label: "Расфасовка",
		autoform: {
			options: function() {
				return _.map(BeerPackings.find().fetch(), function(c, i) {
					return {label: c.title, value: c._id}
				})
			}
		}
	},
	abw: {
		type: String,
		label: "Содержание алкоголя (%)",
		optional: true
	},
	mf: {
		type: String,
		label: "Массовая доля сухих веществ (%)",
		optional: true
	},
	description: {
		type: String,
		label: "Описание",
		optional: true,
		autoform: {
			rows: 3
		}
	},
	isPublish: {
		type: Boolean,
		label: "В публикацию",
		optional: true,
		autoform: {
			type: "hidden"
		}
	},
	photos: {
		type: [String],
		label: "Фотографии"
	},
	'photos.$': {
		autoform: {
			afFieldInput: {
				type: 'fileUpload',
				collection: 'Photos',
				label: "Выбрать файл"
			}
		}
	},
	rating: {
		type: Number,
		decimal: true,
		optional: true,
		autoform: {
			type: "hidden"
		}
	}
});

Beers.attachSchema(BeerSchema);

Beers.allow({
	'insert': function(userId, doc) {
		var user = Meteor.users.findOne({_id: userId});
		if (user && user.profile && (user.profile.role == "admin")) return true;
		return false;
	},
	'update': function(userId, doc, fields, modifier) {
		var user = Meteor.users.findOne({_id: userId});
		if (user && user.profile && (user.profile.role == "admin")) return true;
		return false;
	},
	'remove': function(userId, doc) {
		return false;
	}
});

var resizePhoto = function(fileObj, readStream, writeStream) {
	var sizeX = '200',
		sizeYP = '400^',
		sizeY = '400';
	gm(readStream).autoOrient().resize(sizeX, sizeYP).gravity('Center').extent(sizeX, sizeY).stream('PNG').pipe(writeStream);
};

var createSquareThumb = function(fileObj, readStream, writeStream) {
	var size = '96',
		sizeP = '96^';
	gm(readStream).autoOrient().resize(size, sizeP).gravity('Center').extent(size, size).stream('PNG').pipe(writeStream);
};

Photos = new FS.Collection("photos", {
	stores: [
		new FS.Store.FileSystem("photos", { transformWrite: resizePhoto }),
		new FS.Store.FileSystem("thumbs", { transformWrite: createSquareThumb })
	],
	filter: {
		allow: {
			contentTypes: ['image/*']
		}
	}
});

Photos.allow({
	insert: function(userId, doc) {
		var user = Meteor.users.findOne({_id: userId});
		if (user && user.profile && (user.profile.role == "admin")) return true;
		return false;
	},
	update: function(userId, doc, fields, modifier) {
		var user = Meteor.users.findOne({_id: userId});
		if (user && user.profile && (user.profile.role == "admin")) return true;
		return false;
	},
	download: function(userId, fileObj) {
		return true;
	}
});

BeerTastes = new Mongo.Collection("beertastes");

BeerTasteSchema = new SimpleSchema({
	beer: {
		type: String,
		autoform: {
			type: "hidden"
		}
	},
	author: {
		type: String,
		autoform: {
			type: "hidden"
		}
	},
	flavor: {
		type: String,
		label: "Аромат",
		optional: true,
		autoform: {
			rows: 3
		}
	},
	colour: {
		type: String,
		label: "Цвет",
		optional: true,
		autoform: {
			rows: 3
		}
	},
	opacity: {
		type: String,
		label: "Прозрачность",
		optional: true,
		autoform: {
			rows: 3
		}
	},
	cream: {
		type: String,
		label: "Пеностойкость",
		optional: true,
		autoform: {
			rows: 3
		}
	},
	bitterness: {
		type: String,
		label: "Горечь",
		optional: true,
		autoform: {
			rows: 3
		}
	},
	body: {
		type: String,
		label: "Тело",
		optional: true,
		autoform: {
			rows: 3
		}
	},
	aftertaste: {
		type: String,
		label: "Послевкусие",
		optional: true,
		autoform: {
			rows: 3
		}
	},
	overallImpression: {
		type: String,
		label: "Итоговое впечатление",
		optional: true,
		autoform: {
			rows: 3
		}
	}
});

BeerTastes.attachSchema(BeerTasteSchema);

BeerTastes.allow({
	'insert': function(userId, doc) {
		var user = Meteor.users.findOne({_id: userId});
		if (user && user.profile && (user.profile.role == "admin")) return true;
		return false;
	},
	'update': function(userId, doc, fields, modifier) {
		var user = Meteor.users.findOne({_id: userId});
		if (user && user.profile && (user.profile.role == "admin")) return true;
		return false;
	},
	'remove': function(userId, doc) {
		return false;
	}
});