Meteor.publish("countries", function() {
	return Countries.find();
});

Meteor.publish("beerTypes", function() {
	return BeerTypes.find();
});

Meteor.publish("beerPackings", function() {
	return BeerPackings.find();
});

Meteor.publish("beers", function() {
	return Beers.find();
});

Meteor.publish("beerTastes", function() {
	return BeerTastes.find();
});

Meteor.publish("photos", function() {
	return Photos.find();
});

Meteor.publish("beerById", function(beerId) {
	return Beers.find({_id: beerId});
});

Meteor.publish("beerTasteById", function(beerTasteId) {
	return BeerTastes.find({_id: beerTasteId});
});
