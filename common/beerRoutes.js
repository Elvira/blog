Router.route("/beers/edit/:_id?",
	{
		name: "editBeer",
		path: "/beers/edit/:_id?",
		template: "editBeer",
		subscriptions: function() {
			this.subscribe("countries").wait();
			this.subscribe("beerTypes").wait();
			this.subscribe("photos").wait();
			this.subscribe("beerPackings").wait();
			if (this.params._id) this.subscribe("beerById", this.params._id).wait();
		}
	}
);

Router.route("/beers/types",
	{
		name: "beerTypesList",
		path: "/beers/types",
		template: "beerTypesList",
		subscriptions: function() {
			this.subscribe("beerTypes").wait();
		}
	}
);

Router.route("/beers/countries",
	{
		name: "countriesList",
		path: "/beers/countries",
		template: "countriesList",
		subscriptions: function() {
			this.subscribe("countries").wait();
		}
	}
);

Router.route("/beers/packings",
	{
		name: "beerPackingsList",
		path: "/beers/packings",
		template: "beerPackingsList",
		subscriptions: function() {
			this.subscribe("beerPackings").wait();
		}
	}
);

Router.route("/beers/:beerId/tastes/:_id?",
	{
		name: "beerTaste",
		path: "/beers/:beerId/tastes/:_id?",
		template: "beerTaste",
		subscriptions: function() {
			this.subscribe("beerById", this.params.beerId).wait()
			if (this.params._id) this.subscribe("beerTasteById", this.params._id).wait();
		}
	}
);