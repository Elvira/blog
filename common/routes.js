Router.configure({
	layoutTemplate: 'layout'
});

Router.route("/",
	{
		name: "start",
		path: "/",
		template: "start",
		subscriptions: function() {
			this.subscribe("beerTypes").wait();
			this.subscribe("countries").wait();
			this.subscribe("beerPackings").wait();
			this.subscribe("photos").wait();
			this.subscribe("beers").wait();
			this.subscribe("beerTastes").wait();
		}
	}
);
