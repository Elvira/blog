Template.start.helpers({
	beers: function() {
		if (Meteor.user() && Meteor.user().profile && (Meteor.user().profile.role === "admin")) {
			return Beers.find({}, {sort: {title: 1}});
		} else {
			return Beers.find({isPublish: true}, {sort: {title: 1}});
		}
	},
	canEdit: function() {
		var beer = Beers.findOne({_id: this._id});
		if (Meteor.user() && Meteor.user().profile && (Meteor.user().profile.role === "admin")) {
			return true;
		}
		return false;
	},
	isPublishColor: function() {
		if (this.isPublish) {
			return "";
		}
		return "notPublish";
	},
	photo: function() {
		if (this.photos) {
			return this.photos[0];
		}
	}
});

Template.start.events({
	'touchstart .edit': function() {
		Router.go("/beers/edit/"+this._id);
	},
	'touchstart .view': function() {
		var taste = BeerTastes.findOne({beer: this._id, author: Meteor.userId()});
		if (taste) {
			Router.go("/beers/"+this._id+"/tastes/"+taste._id);
		} else {
			Router.go("/beers/"+this._id+"/tastes");
		}
	},
	'touchstart .publish': function() {
		Beers.update({_id: this._id}, {$set: {isPublish: (!this.isPublish)}});
	},
	'click .edit': function() {
		Router.go("/beers/edit/"+this._id);
	},
	'click .view': function() {
		var taste = BeerTastes.findOne({beer: this._id, author: Meteor.userId()});
		if (taste) {
			Router.go("/beers/"+this._id+"/tastes/"+taste._id);
		} else {
			Router.go("/beers/"+this._id+"/tastes");
		}
	},
	'click .publish': function() {
		Beers.update({_id: this._id}, {$set: {isPublish: (!this.isPublish)}});
	}
});
