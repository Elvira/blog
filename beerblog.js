if (Meteor.isClient) {
	Accounts.ui.config({
		requestPermissions: {
			facebook: ['email', 'user_photos', 'public_profile', 'user_likes'],
		}
	});
	Router.onBeforeAction(function() {
		if (Session.get("login")) {
			if (!Meteor.userId()) {
				this.render("sign");
			} else {
				Session.set("login", false);
				this.next();
			}
		} else {
			this.next();
		}
	});
	/*
	ServiceConfiguration.configurations.upsert(
		{ service: "facebook" },
		{
			$set: {
				clientId: "1654210224836276",
				//loginStyle: "popup",
				secret: "dc75a1a2d26a531d4f92dd9800d44a68"
			}
		}
	);
	*/
}

if (Meteor.isServer) {
	Meteor.startup(function () {
		Meteor.users.find().observeChanges({
			added: function(id, user) {
				var email1 = 'r.prylypko@gmail.com',
					user1 = Meteor.users.findOne({
						$and: [
							{_id: id},
							{$or: [
								{'emails.address': email1},
								{'services.facebook.email': email1}
							]}
						]
					});
				if (user1) {
					Meteor.users.update({_id: id}, {$set: {'profile.role': 'admin'}});
				} else {
					var email2 = 'karashistka@yandex.ru',
						user2 = Meteor.users.findOne({
							$and: [
								{_id: id},
								{$or: [
									{'emails.address': email2},
									{'services.facebook.email': email2}
								]}
							]
						});
					if (user2) {
						Meteor.users.update({_id: id}, {$set: {'profile.role': 'admin'}});
					}
				}
			}
		})
	});
}
